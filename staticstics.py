from os import walk
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

def read_dataframes(libdirname):
    res = []
    for _, _, filenames in walk( libdirname ):
        for fname in filenames:
            if fname.endswith( 'png' ):
                tbl = pd.read_csv( libdirname + "/" + fname + ".rect", delim_whitespace = True,
                                   names = ["class", "x1", "y1", "x2", "y2"],
                                   index_col = None)
                res.append(tbl)
    return pd.concat(res, axis=0, ignore_index=True)

tbl = read_dataframes("raw_planes")

m = pd.concat([tbl["class"], tbl["x2"] - tbl["x1"], tbl["y2"] - tbl["y1"]], axis=1)
m.columns = ["class", "width", "height"]

print(m)

m.hist(bins=100)
plt.show()