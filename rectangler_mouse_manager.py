from PyQt5 import QtWidgets, QtCore, QtGui

# предок управления мышью
class MouseManager:
    def __init__(self, graphicsview):
        self._view = graphicsview
        self._scene = graphicsview.scene()

    def onMouseMove(self, event):
        pass

    def onMousePressed(self, event):
        pass

    def onMouseReleased(self, event):
        pass

    def onEditingModeEnter(self):
        pass

    def onEditingModeReleased(self):
        pass


# управление мышой в режиме навигации
class MouseManagerNavigation( MouseManager ):
    def __init__(self, graphicsview):
        super().__init__( graphicsview )
        self._view.setDragMode( QtWidgets.QGraphicsView.ScrollHandDrag )
        QtGui.QGuiApplication.restoreOverrideCursor()

        # 0 - emptiness, 1 top, 2 right, 3 bottom, 4 left
        self._mouse_place = 0


    def onMouseMove(self, event):
        if not self._scene:
            return

        selecteds = self._scene.selectedItems()
        if not selecteds or len(selecteds) != 1:
            return

        pt = self._view.mapToScene(event.pos())

        radius = 10
        r = selecteds[0].rect()
        if QtCore.QRectF(r.left()-radius, r.top()-radius, r.width() + 2*radius, 2*radius).contains(pt):
            mouse_place = 1
        elif QtCore.QRectF( r.left() - radius, r.bottom() - radius, r.width() + 2*radius, 2*radius ).contains( pt ):
            mouse_place = 3
        elif QtCore.QRectF( r.right() - radius, r.top() - radius, 2*radius, r.height() + 2*radius ).contains( pt ):
            mouse_place = 2
        elif QtCore.QRectF( r.left() - radius, r.top() - radius, 2*radius, r.height() + 2*radius ).contains( pt ):
            mouse_place = 4
        else:
            mouse_place = 0

        if mouse_place != self._mouse_place:
            self._mouse_place = mouse_place
            if mouse_place == 0:
                QtGui.QGuiApplication.restoreOverrideCursor()
            elif mouse_place == 1 or mouse_place == 3:
                QtGui.QGuiApplication.setOverrideCursor(QtCore.Qt.SizeVerCursor)
            else:
                QtGui.QGuiApplication.setOverrideCursor(QtCore.Qt.SizeHorCursor)


    def onMousePressed(self, event):
        if self._mouse_place > 0:
            self._view.changeMouseManager( MouseManagerEdgeMover( self._view, self._mouse_place ) )


    def onEditingModeEnter(self):
        self._view.changeMouseManager(MouseManagerEditingIdle(self._view))


# управление в режиме редактирование (зажали Ctrl)
class MouseManagerEditingIdle( MouseManager ):
    def __init__(self, graphicsview):
        super().__init__( graphicsview )
        self._view.setDragMode( QtWidgets.QGraphicsView.NoDrag )
        QtGui.QGuiApplication.restoreOverrideCursor()
        self._cursor_above_emptiness = True


    def onMousePressed(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            if self._cursor_above_emptiness:
                self._view.changeMouseManager(MouseManagerRectCreating(self._view, event.pos()))

    def onEditingModeReleased(self):
        self._view.changeMouseManager(MouseManagerNavigation(self._view))


# режим Ввод прямоугольника
class MouseManagerRectCreating( MouseManager ):
    def __init__(self, graphicsview, first_point):
        super().__init__( graphicsview )

        pt = self._view.mapToScene( first_point )
        self._first_point = pt

        newrect = self._view.newRect( QtCore.QRectF( pt.x(), pt.y(), 1, 1 ) )

        self._current_rect = newrect


    def onMouseMove(self, event):
        pt1 = self._first_point
        pt2 = self._view.mapToScene(event.pos())

        min_x = min(pt1.x(), pt2.x())
        min_y = min(pt1.y(), pt2.y())
        max_x = max(pt1.x(), pt2.x())
        max_y = max(pt1.y(), pt2.y())

        self._current_rect.setRect( min_x, min_y, max_x - min_x, max_y - min_y )


    def onMouseReleased(self, event):
        self._current_rect.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable, True)
        self._current_rect.setBrush(QtGui.QBrush(QtGui.QColor(100, 100, 100, 50)))

        if QtGui.QGuiApplication.keyboardModifiers() == QtCore.Qt.ControlModifier:
            self._view.changeMouseManager(MouseManagerEditingIdle(self._view))
        else:
            self._view.changeMouseManager(MouseManagerNavigation(self._view))


class MouseManagerEdgeMover(MouseManager):
    def __init__(self, graphicsview, edge_index):
        self._view = graphicsview
        self._scene = graphicsview.scene()
        self._edge_index = edge_index
        self._view.setDragMode( QtWidgets.QGraphicsView.NoDrag )


    def onMouseMove(self, event):
        pt = self._view.mapToScene( event.pos() )
        selecteds = self._scene.selectedItems()

        if len(selecteds) != 1:
            return

        item = selecteds[0]
        r = item.rect()

        if self._edge_index == 1:
            r.setTop(pt.y())
        elif self._edge_index == 2:
            r.setRight(pt.x())
        elif self._edge_index == 3:
            r.setBottom(pt.y())
        else:
            r.setLeft(pt.x())

        if r.width() > 0 and r.height() > 0:
            item.setRect(r)


    def onMouseReleased(self, event):
        if QtGui.QGuiApplication.keyboardModifiers() == QtCore.Qt.ControlModifier:
            self._view.changeMouseManager(MouseManagerEditingIdle(self._view))
        else:
            self._view.changeMouseManager(MouseManagerNavigation(self._view))
