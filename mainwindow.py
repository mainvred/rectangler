import sys

from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QMainWindow, QMessageBox, QStyle


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        uic.loadUi("mainwindow.ui", self )

        self._filename = ''

        self.graphicsView.selectedItemChanged.connect(self.onSelectedItemChanged)
#        self.graphicsView.onShowMainMenu.connect(self.showMainMenu)

        self.makeToolbar()

        self.graphicsView.show()


    def onSelectedItemChanged(self, item_type):
        types = ['unk', 'Am', 'USB', 'LSB', 'Fm', 'Cw']
        self.statusBar.showMessage(types[item_type])


    def getIcon(self, icon_name):
        return self.style().standardIcon( getattr( QStyle, icon_name ) )


    def makeToolbar(self):
        action_save = self.toolBar.addAction(self.getIcon('SP_DialogSaveButton'), 'Сохранить')
        action_save.setShortcut('F2')
        action_load = self.toolBar.addAction(self.getIcon('SP_FileDialogStart'), 'Загрузить')
        action_load.setShortcut('F3')

        action_save.triggered.connect(self.onStore)
        action_load.triggered.connect(self.onLoad)


    def onLoad(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self,
                                                            "Картинка со спектральным дождиком",
                                                            "",
                                                            "All Files (*);;Signal Pictures (*.png);;Signal Pictures (*.jpg)",
                                                            options=options)
        if not fileName:
            return

        if self.graphicsView.hasChanges():
            res = QMessageBox().question( self, "Внимание!", 'Все изменения будут потеряны. Продолжить?',
                                                  QMessageBox.Yes | QMessageBox.No, QMessageBox.No )
            if res == QMessageBox.No:
                return

        self._filename = fileName
        self.graphicsView.restore(fileName)


    def onStore(self):
        self.graphicsView.store(self._filename)


    def closeEvent(self, event):
        if not self.graphicsView.hasChanges():
            event.accept()
            return

        reply = QMessageBox().question( self, "Внимание!", 'Все изменения будут потеряны. Продолжить?',
                                        QMessageBox.Save | QMessageBox.Close | QMessageBox.Cancel,
                                        QMessageBox.Save)

        if reply == QMessageBox.Close:
            event.accept()
        elif reply == QMessageBox.Save:
            self.onStore()
            event.accept()
        else:
            event.ignore()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mw = MainWindow()
    mw.show()

    sys.exit( app.exec_() )
