from PyQt5 import QtWidgets
from PyQt5 import QtCore, QtGui
from enum import Enum
import rectangler_mouse_manager as rmm

from PyQt5.QtWidgets import QStyle


class _EditMode(Enum):
    navigation = 1,
    putting_first_point = 2,
    dragging_second_point = 3


class _Rectangle(QtWidgets.QGraphicsRectItem):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.modulation = 0


class RectangleGraphEditor(QtWidgets.QGraphicsView):
    photoClicked = QtCore.pyqtSignal(QtCore.QPoint)
    selectedItemChanged = QtCore.pyqtSignal(int)
    onShowMainMenu = QtCore.pyqtSignal(QtGui.QContextMenuEvent)

    def __init__(self, parent):
        super(RectangleGraphEditor, self).__init__(parent)
        self._edit_mode = _EditMode.navigation
        self._zoom = 0
        self._empty = True
        self._scene = QtWidgets.QGraphicsScene(self)
        self._scene.selectionChanged.connect(self.selectionChanged)
        self._photo = QtWidgets.QGraphicsPixmapItem()
        self._current_rect = None
        self._first_point = QtCore.QPoint()
        self._has_changes = False
        self._mouseManager = rmm.MouseManagerNavigation(self)
        self.setScene(self._scene)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setBackgroundBrush(QtGui.QBrush(QtGui.QColor(30, 30, 30)))
        self.setFrameShape(QtWidgets.QFrame.NoFrame)


    def changeMouseManager(self, manager):
        self._mouseManager = manager


    def hasPhoto(self):
        return not self._empty


    def resizeEvent(self, event):
        self.fitInView()
        return super().resizeEvent(event)


    def fitInView(self, scale=True):
        rect = QtCore.QRectF(self._photo.pixmap().rect())
        if not rect.isNull():
            self.setSceneRect(rect)
            if self.hasPhoto():
                unity = self.transform().mapRect(QtCore.QRectF(0, 0, 1, 1))
                self.scale(1 / unity.width(), 1 / unity.height())
                viewrect = self.viewport().rect()
                scenerect = self.transform().mapRect(rect)
                factor = min(viewrect.width() / scenerect.width(),
                             viewrect.height() / scenerect.height())
                self.scale(factor, factor)
            self._zoom = 0


    def hasChanges(self):
        return self._has_changes


    def setPhoto(self, pixmap):
        self._zoom = 0
        if pixmap and not pixmap.isNull():
            self._empty = False
            self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)
            self._photo = QtWidgets.QGraphicsPixmapItem()
            self._photo.setPixmap(pixmap)
            self._scene.addItem(self._photo)
            self._photo.setZValue(-10000)

        self.fitInView()


    def wheelEvent(self, event):
        if self.hasPhoto():
            if event.angleDelta().y() > 0:
                factor = 1.25
                self._zoom += 1
            else:
                factor = 0.8
                self._zoom -= 1
            if self._zoom > 0:
                self.scale(factor, factor)
            elif self._zoom == 0:
                self.fitInView()
            else:
                self._zoom = 0


    def mousePressEvent(self, event):
        self._mouseManager.onMousePressed(event)

        return super().mousePressEvent( event )


    def mouseReleaseEvent(self, event):
        self._mouseManager.onMouseReleased(event)

        return super().mouseReleaseEvent(event)


    def mouseMoveEvent(self, event):
        self._mouseManager.onMouseMove(event)

        return super(RectangleGraphEditor, self).mouseMoveEvent(event)


    def keyPressEvent(self, event):
        event_finished = False

        if int(event.modifiers()) == QtCore.Qt.ControlModifier:
            self._mouseManager.onEditingModeEnter()

        if event.key() == QtCore.Qt.Key_Delete:
            self.deleteSelectedRect()

        if event.key() == QtCore.Qt.Key_Escape:
            self._photo.setZValue( -10000 if self._photo.zValue() > 0 else 10000 )

        if event.key() in ( QtCore.Qt.Key_Left, QtCore.Qt.Key_Right, QtCore.Qt.Key_Up, QtCore.Qt.Key_Down ):
            event_finished = self.arrowPressed(event.key())

        if not event_finished:
            return super(RectangleGraphEditor, self).keyPressEvent(event)


    def keyReleaseEvent(self, event):
        if int(event.modifiers()) != QtCore.Qt.ControlModifier:
            self._mouseManager.onEditingModeReleased()

        return super(RectangleGraphEditor, self).keyReleaseEvent(event)


    def changeMode(self, mode):
        self._edit_mode = mode

        if mode == _EditMode.navigation:
            self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)
        else:
            self.setDragMode(QtWidgets.QGraphicsView.NoDrag)


    def newRect(self, rect):
        newrect = _Rectangle()
        newrect.setPen(QtGui.QPen(QtGui.QColor(255, 0, 0)))
        newrect.setRect(rect)
        self._scene.addItem( newrect )
        self._has_changes = True
        return newrect


    def deleteSelectedRect(self):
        selecteds = self._scene.selectedItems()
        if not selecteds:
            return

        for item in selecteds:
            self._scene.removeItem( item )


    def selectionChanged(self):
        selecteds = self._scene.selectedItems()
        if len(selecteds) == 1:
            self.selectedItemChanged.emit( selecteds[0].modulation )


    def arrowPressed(self, arrowkey):
        selected = self._scene.selectedItems()

        if len(selected) != 1:
            return False

        if not self.underMouse():
            return False

        selected = selected[0]

        mouse_pos = QtGui.QCursor.pos()
        mouse_pos = self.mapFromGlobal(mouse_pos)
        mouse_pos = self.mapToScene(mouse_pos)

        center = selected.rect().center()
        rect = selected.rect()

        if arrowkey == QtCore.Qt.Key_Up:
            if mouse_pos.y() < center.y():
                rect.setTop( rect.top() - 1 )
            else:
                rect.setBottom( rect.bottom() - 1 )
        elif arrowkey == QtCore.Qt.Key_Down:
            if mouse_pos.y() < center.y():
                rect.setTop( rect.top() + 1 )
            else:
                rect.setBottom( rect.bottom() + 1 )
        elif arrowkey == QtCore.Qt.Key_Left:
            if mouse_pos.x() < center.x():
                rect.setLeft( rect.left() - 1 )
            else:
                rect.setRight( rect.right() - 1 )
        else:
            if mouse_pos.x() < center.x():
                rect.setLeft( rect.left() + 1 )
            else:
                rect.setRight( rect.right() + 1 )

        self._has_changes = True
        selected.setRect(rect)

        return True


    def contextMenuEvent(self, event):
        selecteds = self._scene.selectedItems()

        if len(selecteds) == 1:
            self.setRectangleTypeMenu(selecteds[0], event.pos())

        return super(RectangleGraphEditor, self).contextMenuEvent(event)

    def getIcon(self, icon_name):
        return self.style().standardIcon( getattr( QStyle, icon_name ) )


    def setRectangleTypeMenu(self, item, pos):
        menu = QtWidgets.QMenu(self)

        types = ['unk', 'Am', 'USB', 'LSB', 'Fm', 'Cw']
        actions = [ menu.addAction(act) for act in types ]

        actions[ item.modulation ].setIcon( self.style().standardIcon(getattr(QStyle,'SP_MessageBoxWarning')) )

        menu.addSeparator()
        action_del = menu.addAction(self.getIcon('SP_BrowserStop'), 'Удалить')
        action_elongate = menu.addAction(self.getIcon('SP_ArrowUp'), 'Продлить')
        action_back = menu.addAction(self.getIcon('SP_BrowserReload'), 'Отдалить')

        res_action = menu.exec_(self.mapToGlobal(pos))

        if not res_action:
            return

        if res_action == action_del:
            self.deleteSelectedRect()

        if res_action == action_elongate:
            r = item.rect()
            item.setRect( QtCore.QRectF(r.left(), 0, r.width(), self._photo.boundingRect().height()) )

        if res_action in actions and item.modulation != actions.index(res_action):
            item.modulation = actions.index(res_action)
            self.selectedItemChanged.emit( item.modulation )

        if res_action == action_back:
            item.setZValue(item.zValue() - 10)

        self._has_changes = True


    def store(self, pic_filename):
        with open(pic_filename + '.rect', 'w') as f:
            for i in self.items():
                if not isinstance(i, QtWidgets.QGraphicsRectItem):
                    continue

                r = i.rect()
                f.write( '{} {} {} {} {}\n'.format(i.modulation, r.left(), r.top(), r.right(), r.bottom()) )

        self._has_changes = False


    def restore(self, pic_filename):
        self._scene.clear()
        self.setPhoto(QtGui.QPixmap(pic_filename))
        self._photo.setZValue( -10000 )

        try:
            with open(pic_filename + '.rect', 'r') as f:
                for line in f:
                    rect = tuple( [float(i) for i in line.split()] )
                    newobject = self.newRect( QtCore.QRectF( rect[1], rect[2], rect[3] - rect[1], rect[4] - rect[2] ) )
                    newobject.setFlag( QtWidgets.QGraphicsItem.ItemIsSelectable, True )
                    newobject.setBrush( QtGui.QBrush( QtGui.QColor( 100, 100, 100, 50 ) ) )
                    newobject.modulation = int(rect[0] + 0.5)
        except FileNotFoundError:
            pass

        self._has_changes = False
